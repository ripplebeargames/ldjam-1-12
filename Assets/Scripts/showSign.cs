﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class showSign : MonoBehaviour {

	public GameObject text;

	void OnTriggerEnter(Collider player){
		if(player.gameObject.GetComponent<PlayerControllerTransform>()){			
			text.SetActive(true);
		}
	}

	void OnTriggerExit(Collider player){
		if(player.gameObject.GetComponent<PlayerControllerTransform>()){			
			text.SetActive(false);
		}
	}
	
}
