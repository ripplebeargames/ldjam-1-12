﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class victory : MonoBehaviour {

	public GameObject victoryScreen;
	public Image bronze;
	public Image silver;
	public Image gold;
	public static bool GameIsPaused = false;

	void Update(){
		if(victoryScreen.activeSelf){
			if(SceneManager.GetActiveScene().name == "Level2"){
				if(staticController.level2Score.Contains("bronze"))
					bronze.fillAmount += 1.0f / 5.0f * Time.deltaTime;
				if(staticController.level2Score.Contains("silver"))
					silver.fillAmount += 1.0f / 5.0f * Time.deltaTime;
				if(staticController.level2Score.Contains("gold"))
					gold.fillAmount += 1.0f / 5.0f * Time.deltaTime;
			}
			else if(SceneManager.GetActiveScene().name == "MainLevel"){
				if(staticController.level1Score.Contains("bronze"))
					bronze.fillAmount += 1.0f / 5.0f * Time.deltaTime;
				if(staticController.level1Score.Contains("silver"))
					silver.fillAmount += 1.0f / 5.0f * Time.deltaTime;
				if(staticController.level1Score.Contains("gold"))
					gold.fillAmount += 1.0f / 5.0f * Time.deltaTime;
			}
		}
	}

	void OnTriggerEnter(Collider player){
		if(player.gameObject.GetComponent<PlayerControllerTransform>()){
			Pause();
		}
	}

	void Pause(){
		victoryScreen.SetActive(true);
		//Time.timeScale = 0f;
		GameIsPaused = true;
	}

}
