﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GlobalGameManager : MonoBehaviour
{
    public static List<string> coinScore = new List<string>();
    public static Vector3 initialPlayerLocation;
    public static Quaternion initialPlayerRotation;
    public GameObject pauseMenu;
    public static bool GameIsPaused = false;
    private GameObject player;

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Use this for initialization
    void Start()
    {
        initialPlayerLocation = player.transform.position;
        initialPlayerRotation = player.transform.rotation;

        DestroyImmediate(player);
        if(SceneManager.GetActiveScene().name == "Level2")
            player = (GameObject) Instantiate(Resources.Load("Character1"), initialPlayerLocation, initialPlayerRotation);
        else if(SceneManager.GetActiveScene().name == "MainLevel")
            player = (GameObject) Instantiate(Resources.Load("Character2"), initialPlayerLocation, initialPlayerRotation);


//		Debug.Log("init rotation: " + initialPlayerRotation.y);
        //pauseMenu = GameObject.Find("PayseMenuImage");
    }


    void ToggleUI(GameObject ui)
    {
        if (ui.activeSelf)
            ui.SetActive(false);
        else
            ui.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused)
            {
                //Resources.FindObjectsOfTypeAll<AnUniqueClass>();
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    void Resume()
    {
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
    }

    void Pause()
    {
        pauseMenu.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }
}