﻿using System.Collections.Generic;
using UnityEngine;

public class cameraController : MonoBehaviour
{
    private Vector3 offset;
    private Quaternion rotationOffset;
    public static GameObject player;
    public float smoothSpeed = 0.125f;
    public float lookSpeedH = 2f;
    public float lookSpeedV = 2f;
    public float zoomSpeed = 2f;
    public float dragSpeed = 6f;
    public float orbitSpeed = 10.0f;
    private float yaw = 0f;
    private float pitch = 0f;
    public float minZoom = 10f;
    public float maxZoom = 300f;
    private float curZoom = 0f;
    public static float zoomLevel;
    public static float startingZoomLevel;
    private bool lookAt = true;

    // camera movement
    public enum RotationAxes
    {
        MouseXAndY = 0,
        MouseX = 1,
        MouseY = 2
    }

    public RotationAxes axes = RotationAxes.MouseXAndY;
    public float sensitivityX = 15F;
    public float sensitivityY = 15F;

    public float minimumX = -360F;
    public float maximumX = 360F;

    public float minimumY = -60F;
    public float maximumY = 60F;

    float rotationX = 0F;
    float rotationY = 0F;

    private List<float> rotArrayX = new List<float>();
    float rotAverageX = 0F;

    private List<float> rotArrayY = new List<float>();
    float rotAverageY = 0F;

    public float frameCounter = 20;

    Quaternion originalRotation, originalPlayerRotation;

    private GameObject playerGameObject;

    private void Awake()
    {
//        playerGameObject = GameObject.FindGameObjectWithTag("Player");
//        transform.localRotation = Quaternion.Euler(0, 0, 0);
    }

    // Use this for initialization
    void Start()
    {
//        player = GameObject.Find("Character");
//        offset = transform.position - player.transform.position;
//        rotationOffset = transform.rotation * player.transform.rotation;
//        Vector3 pos = transform.position;
//        startingZoomLevel = pos.y;

        Rigidbody rb = GetComponent<Rigidbody>();
        if (rb)
            rb.freezeRotation = true;
        
        originalRotation = transform.localRotation;
        originalPlayerRotation = transform.parent.localRotation;
    }

    void Update()
    {
        rotateCamera();

        //drag camera around with right Mouse
//        if (Input.GetMouseButton(1))
//        {
//            transform.Translate(-Input.GetAxisRaw("Mouse X") * Time.deltaTime * dragSpeed, -Input.GetAxisRaw("Mouse Y") * Time.deltaTime * dragSpeed, 0);
//        }

        //Zoom in and out with Mouse Wheel
//        float scroll = Input.GetAxis("Mouse ScrollWheel");
//
//        Vector3 pos = transform.position;
//
//		pos.y -= scroll * 1000 * zoomSpeed * Time.deltaTime;
//		pos.y = Mathf.Clamp(pos.y, minZoom, maxZoom);
//        zoomLevel = pos.y;
//
//        transform.position = pos;
    }

    // Update is called once per frame
    void LateUpdate()
    {
//		transform.position = player.transform.position + offset;
//        transform.rotation = player.transform.rotation;
//		transform.Rotate(20,0,0);
    }

    static void setPlayer(GameObject newPlayer)
    {
        player = newPlayer;
    }

    static GameObject getPlayer()
    {
        return player;
    }

    public static float ClampAngle(float angle, float min, float max)
    {
        angle = angle % 360;
        if ((angle >= -360F) && (angle <= 360F))
        {
            if (angle < -360F)
            {
                angle += 360F;
            }

            if (angle > 360F)
            {
                angle -= 360F;
            }
        }

        return Mathf.Clamp(angle, min, max);
    }

    private void rotateCamera()
    {
        if (axes == RotationAxes.MouseXAndY)
        {
            rotAverageY = 0f;
            rotAverageX = 0f;

            rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
            rotationX += Input.GetAxis("Mouse X") * sensitivityX;

            rotArrayY.Add(rotationY);
            rotArrayX.Add(rotationX);

            if (rotArrayY.Count >= frameCounter)
            {
                rotArrayY.RemoveAt(0);
            }

            if (rotArrayX.Count >= frameCounter)
            {
                rotArrayX.RemoveAt(0);
            }

            for (int j = 0; j < rotArrayY.Count; j++)
            {
                rotAverageY += rotArrayY[j];
            }

            for (int i = 0; i < rotArrayX.Count; i++)
            {
                rotAverageX += rotArrayX[i];
            }

            rotAverageY /= rotArrayY.Count;
            rotAverageX /= rotArrayX.Count;

            rotAverageY = ClampAngle(rotAverageY, minimumY, maximumY);
            rotAverageX = ClampAngle(rotAverageX, minimumX, maximumX);

            Quaternion yQuaternion = Quaternion.AngleAxis(rotAverageY, Vector3.left);
            Quaternion xQuaternion = Quaternion.AngleAxis(rotAverageX, Vector3.up);

            transform.localRotation = originalRotation * yQuaternion;
            transform.parent.transform.localRotation = originalPlayerRotation * xQuaternion;
           
        }
        else if (axes == RotationAxes.MouseX)
        {
            rotAverageX = 0f;

            rotationX += Input.GetAxis("Mouse X") * sensitivityX;

            rotArrayX.Add(rotationX);

            if (rotArrayX.Count >= frameCounter)
            {
                rotArrayX.RemoveAt(0);
            }

            for (int i = 0; i < rotArrayX.Count; i++)
            {
                rotAverageX += rotArrayX[i];
            }

            rotAverageX /= rotArrayX.Count;

            rotAverageX = ClampAngle(rotAverageX, minimumX, maximumX);

            Quaternion xQuaternion = Quaternion.AngleAxis(rotAverageX, Vector3.up);
//            transform.localRotation = originalRotation * xQuaternion;
            transform.parent.transform.localRotation = originalPlayerRotation * xQuaternion;
        }
        else
        {
            rotAverageY = 0f;

            rotationY += Input.GetAxis("Mouse Y") * sensitivityY;

            rotArrayY.Add(rotationY);

            if (rotArrayY.Count >= frameCounter)
            {
                rotArrayY.RemoveAt(0);
            }

            for (int j = 0; j < rotArrayY.Count; j++)
            {
                rotAverageY += rotArrayY[j];
            }

            rotAverageY /= rotArrayY.Count;

            rotAverageY = ClampAngle(rotAverageY, minimumY, maximumY);

            Quaternion yQuaternion = Quaternion.AngleAxis(rotAverageY, Vector3.left);
            transform.localRotation = originalRotation * yQuaternion;
        }
    }
}