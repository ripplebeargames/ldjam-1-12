﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fanRotate : MonoBehaviour
{
    public bool isEnabled;
    public float RotationSpeed = 0.01f;

    // Use this for initialization
    void Start()
    {
        isEnabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (isEnabled)
        {
            //create rotation
            if (transform.eulerAngles.x < 360)
                transform.Rotate(5f, 0, 0);
            else
                transform.Rotate(0, 0, 0);
        }
    }

    public void toggleFanRotate()
    {
        if (isEnabled == true)
            isEnabled = false;
        else if (isEnabled == false)
        {
            Debug.Log("false");
            isEnabled = true;
        }
    }

    void OnTriggerEnter(Collider player)
    {
        if (player.gameObject.GetComponent<PlayerControllerTransform>())
        {
            if (isEnabled)
            {
                player.GetComponent<Rigidbody>().AddForce(new Vector3(-1000, 0, -1000));
            }
        }
    }
}