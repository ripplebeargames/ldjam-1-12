﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class leverForSpikes : MonoBehaviour {

	public GameObject handle;
	public GameObject spikes;
	public AudioClip alarm;
	public Vector3 offset;
	public Vector3 negativeOffset;
	private bool isOut = false;
	private AudioSource source;

	void Start(){
		source = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerStay(Collider player){
		if(player.gameObject.GetComponent<PlayerControllerTransform>()){			
			if(Input.GetKeyDown(KeyCode.E)){
				if(!isOut){
					source.PlayOneShot (alarm);
					spikes.transform.position = spikes.transform.position + offset;
					isOut = true;
					//float angle = Quaternion.Angle(handle.transform.rotation, Quaternion.Euler(45,0,0) );
					if( handle.transform.eulerAngles.x == 0)
						handle.transform.Rotate( new Vector3(45,0,0) );
					if( handle.transform.eulerAngles.x == 45.0f)
						handle.transform.Rotate( new Vector3(-45,0,0) );
				}
				else{
					source.PlayOneShot (alarm);
					spikes.transform.position = spikes.transform.position + negativeOffset;
					isOut = false;
					//float angle = Quaternion.Angle(handle.transform.rotation, Quaternion.Euler(45,0,0) );
					if( handle.transform.eulerAngles.x == 0)
						handle.transform.Rotate( new Vector3(45,0,0) );
					if( handle.transform.eulerAngles.x == 45.0f)
						handle.transform.Rotate( new Vector3(-45,0,0) );
				}
			}
		}
	}
}
