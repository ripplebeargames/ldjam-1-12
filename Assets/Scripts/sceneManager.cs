﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class sceneManager : MonoBehaviour {

	public void newGame(){
		SceneManager.LoadScene("MainLevel");
	}

	public void level2(){
		SceneManager.LoadScene("Level2");
	}

	public void mainMenu(){
		SceneManager.LoadScene("MainMenu");
	}

	public void exitGame(){
		Application.Quit();
	}

}
