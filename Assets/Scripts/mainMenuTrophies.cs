﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class mainMenuTrophies : MonoBehaviour {

	public GameObject l1b;
	public GameObject l1s;
	public GameObject l1g;
	public GameObject l2b;
	public GameObject l2s;
	public GameObject l2g;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(staticController.level1Score.Contains("bronze"))
			l1b.SetActive(true);
		if(staticController.level1Score.Contains("silver"))
			l1s.SetActive(true);
		if(staticController.level1Score.Contains("gold"))
			l1g.SetActive(true);
		if(staticController.level2Score.Contains("bronze"))
			l2b.SetActive(true);
		if(staticController.level2Score.Contains("silver"))
			l2s.SetActive(true);
		if(staticController.level2Score.Contains("gold"))
			l2g.SetActive(true);
	}
}
