﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class openDoor : MonoBehaviour {

	public GameObject connectedDoor;
	public GameObject handle;

	void OnTriggerStay(Collider player){

		if(player.gameObject.GetComponent<PlayerControllerTransform>()){			
			if(Input.GetKeyDown(KeyCode.E)){
				toggleActive(connectedDoor);
			}
		}

	}

	void toggleActive(GameObject door){
		if(door.activeSelf){
			door.SetActive(false);
			handle.transform.Rotate( new Vector3(45,0,0) );
		}
		else{
			door.SetActive(true);
			handle.transform.Rotate( new Vector3(-45,0,0) );
		}
	}

}
