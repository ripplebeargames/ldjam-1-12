﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerTransform : MonoBehaviour {

	private string MoveInputAxisY = "Vertical";
	private string MoveInputAxisX = "Horizontal";

    public bool isGrounded;
	public Vector3 jump;
	public float jumpSpeed;
    public float fallMultiplier = 2.5f;
    public float lowJumpMultiplier = 2f;
    Rigidbody rb;
	private Vector3 moveDirection = Vector3.zero;

	private CharacterController controller;
	
    // rotation that occurs in angles per second holding down input
    public float rotationRate;

    // units moved per second holding down move input
    public float moveSpeed;
	
	public float gravity;

	// Update is called once per frame
	private void Update () 
    {
//		float moveAxis = Input.GetAxis(MoveInputAxis);
//		float turnAxis = Input.GetAxis(TurnInputAxis);

	    if (controller.isGrounded)
	    {
		    moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
		    moveDirection = transform.TransformDirection(moveDirection);
		    moveDirection = moveDirection * moveSpeed;
		    
		    if (Input.GetButton("Jump"))
		    {
			    moveDirection.y = jumpSpeed;
		    }
	    }

	    /* THIS CONTROLLER MOVES IN 2 AXES */
        // var x = Input.GetAxis("Horizontal") * Time.deltaTime * 10.0f;
        // var z = Input.GetAxis("Vertical") * Time.deltaTime * 10.0f;

        // transform.Translate(x, 0, 0);
        // transform.Translate(0, 0, z);
	    
	    // Apply gravity
	    moveDirection.y = moveDirection.y - (gravity * Time.deltaTime);
	    
	    controller.Move(moveDirection * Time.deltaTime);

	    
	    
//        if(Input.GetKeyDown(KeyCode.Space) && isGrounded){
//            isGrounded = false;
//            rb.velocity = Vector3.up * jumpSpeed;
//            if(rb.velocity.y < 0){
//                rb.velocity += Vector3.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
//            }
//            else if( rb.velocity.y > 0 && !Input.GetKey(KeyCode.Space) )
//                rb.velocity += Vector3.up * Physics2D.gravity.y * (lowJumpMultiplier - 1) * Time.deltaTime;
//        	//rb.AddForce(jump * jumpSpeed, ForceMode.Impulse);            
//        }

//        ApplyInput(moveAxis, turnAxis);
	}

    void Start () {
//        rb = GetComponent<Rigidbody>();
//        jump = new Vector3(0.0f, 2.0f, 0.0f);
	    controller = GetComponent<CharacterController>();
    }

    void OnCollisionEnter()
	{
		isGrounded = true;
	}

    

    private void ApplyInput(float moveInput,
                            float turnInput) 
    {
		Move(moveInput);
//		Turn(turnInput);
    }

    private void Move(float input) 
    {
		transform.Translate(Vector3.forward * input * moveSpeed);
	}

    private void Turn(float input)
    {
//        transform.Rotate(0, input * rotationRate * Time.deltaTime, 0);
	    transform.localRotation = Quaternion.Euler(0,input * rotationRate * Time.deltaTime, 0 ); 
    }
}