﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class leverFan : MonoBehaviour
{
    public GameObject handle;
    public GameObject fan;
    public AudioClip alarm;
    private AudioSource source;
    private float prevTime;

    void Start()
    {
        source = GetComponent<AudioSource>();
        prevTime = 0;
    }

    // Update is called once per frame
    void Update()
    {
    }

    void OnTriggerStay(Collider player)
    {
        Debug.Log("Time dif: " + (Time.time - prevTime));


        if (player.gameObject.GetComponent<PlayerControllerTransform>())
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                if ((Time.time - prevTime) * 1000 <= 200)
                    return;
                source.PlayOneShot(alarm);
                Debug.Log("Hello");
                fan.GetComponent<fanRotate>().toggleFanRotate();
                //float angle = Quaternion.Angle(handle.transform.rotation, Quaternion.Euler(45,0,0) );
                Debug.Log(handle.transform.eulerAngles);
                if (handle.transform.eulerAngles.x == 0)
                    handle.transform.Rotate(new Vector3(45, 0, 0));
                if (handle.transform.eulerAngles.x == 45.0f)
                    handle.transform.Rotate(new Vector3(-45, 0, 0));
                prevTime = Time.time;
            }
        }
    }
}