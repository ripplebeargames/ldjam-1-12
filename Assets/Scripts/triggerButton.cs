﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class triggerButton : MonoBehaviour {

	public AudioClip alarm;
	private AudioSource source;
	public GameObject winFlag;
	public GameObject goldCoin;

	void Start(){
		source = GetComponent<AudioSource>();
	}


	void OnTriggerEnter(Collider player){
		if(player.gameObject.GetComponent<PlayerControllerTransform>()){	
			source.PlayOneShot(alarm);
			transform.position -= new Vector3(0,0.5f,0);
			winFlag.SetActive(true);
			goldCoin.SetActive(true);
		}
	}

	void OnTriggerExit(Collider player){
		if(player.gameObject.GetComponent<PlayerControllerTransform>()){	
			source.PlayOneShot(alarm);
			transform.position -= new Vector3(0,-0.5f,0);
			winFlag.SetActive(false);
			goldCoin.SetActive(false);
		}
	}
}
