﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class scoreController : MonoBehaviour {

	public string coinValue;
	public AudioClip alarm;
	private AudioSource source;

	void Start(){
		source = GetComponent<AudioSource>();
	}

	void Update(){
		if(transform.eulerAngles.x < 360)
			transform.Rotate(0, 5f, 0);
		else
			transform.Rotate(0, 0, 0);
	}
	void OnTriggerEnter(Collider player){

		if(player.gameObject.GetComponent<PlayerControllerTransform>()){			
			source.PlayOneShot (alarm);	
			if(SceneManager.GetActiveScene().name == "Level2")
				staticController.level2Score.Add(coinValue);
			if(SceneManager.GetActiveScene().name == "MainLevel")
				staticController.level1Score.Add(coinValue);
			Destroy(gameObject, alarm.length);
		}

	}
}
