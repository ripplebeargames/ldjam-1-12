﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerDeath : MonoBehaviour
{
    public AudioClip alarm;
    private AudioSource source;

    void Start()
    {
//        source = GetComponent<AudioSource>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<PlayerControllerTransform>())
        {
//            source.PlayOneShot(alarm);
            GameObject clonedPlayer = Instantiate(other.gameObject, GlobalGameManager.initialPlayerLocation,
                GlobalGameManager.initialPlayerRotation);
            Destroy(clonedPlayer.transform.GetComponentInChildren<AudioSource>().gameObject);
            other.transform.GetChild(2).GetChild(0).parent = clonedPlayer.transform.GetChild(2);
            Destroy(other.transform.GetChild(2).gameObject);
            Destroy(other.GetComponent<PlayerControllerTransform>());
            other.transform.GetChild(1).gameObject.SetActive(true);
            if (SceneManager.GetActiveScene().name == "Level2")
                other.transform.localRotation = Quaternion.Euler(90, 0, 0);
            else
                other.transform.localRotation = Quaternion.Euler(90, -90, 0);
            other.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
//			cameraController.player = clonedPlayer;
            //float tiltAroundZ = Input.GetAxis("Horizontal") * 60.0f;
//			other.transform.localRotation = Quaternion.Euler(90,0,0);
            //other.transform.position = new Vector3(other.transform.position.x, other.transform.position.y + 5f, other.transform.position.z);			
        }
    }
}