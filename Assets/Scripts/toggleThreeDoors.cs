﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class toggleThreeDoors : MonoBehaviour
{
    public GameObject connectedDoor1;
    public GameObject connectedDoor2;
    public GameObject connectedDoor3;
    public GameObject handle;

    public AudioClip alarm;
    private AudioSource source;
    private float prevTime;

    void Start()
    {
        source = GetComponent<AudioSource>();
    }

    void OnTriggerStay(Collider player)
    {
        if (player.gameObject.GetComponent<PlayerControllerTransform>())
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                if ((Time.time - prevTime) * 1000 <= 200)
                    return;

                source.PlayOneShot(alarm);
                toggleActive(connectedDoor1);
                toggleActive(connectedDoor2);
                toggleActive(connectedDoor3);
                Debug.Log(handle.transform.eulerAngles.x);
                if (handle.transform.eulerAngles.x == 0)
                    handle.transform.Rotate(new Vector3(45, 0, 0));
                if (handle.transform.eulerAngles.x == 45.0f)
                    handle.transform.Rotate(new Vector3(-45, 0, 0));

                prevTime = Time.time;
            }
        }
    }

    void toggleActive(GameObject door)
    {
        if (door.activeSelf)
        {
            door.SetActive(false);
            //handle.transform.Rotate( new Vector3(45,0,0) );
        }
        else
        {
            door.SetActive(true);
            //handle.transform.Rotate( new Vector3(-45,0,0) );
        }
    }
}